<form method="get" class="search-form" id="searchform" action="<?php bloginfo('url'); ?>">
    <div>
        <input class="search-field" type="text" placeholder="<?php _e('Search');?>" name="s" id="s" />
        <button type="submit" class="search-submit" name="submit"><?php _e('Search');?></button>
    </div>
</form>