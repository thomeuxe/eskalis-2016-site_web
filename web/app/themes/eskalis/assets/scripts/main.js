/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

// Functions

// Functions

function wpcf7Submitted(e) {
    var el = document.querySelector('#main .wpcf7 [type="submit"]');
    el.innerHTML = "Message envoyé";

    if (el.classList)
        el.classList.add("wpcf7-mail-sent-ok");
    else
        el.className += ' ' + "wpcf7-mail-sent-ok";
}

var geocoder, map;

function codeAddress(address, map) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'address': address
    }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var myOptions = {
                zoom: 12,
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(map, myOptions);

            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                icon: document.location.origin + "/app/themes/eskalis/assets/images/icons/pointer.svg"
            });
        }
    });
}

function initContactMap() {
    codeAddress("3Bis Impasse Les Prairies, 74940 Annecy-le-Vieux", document.getElementById('contactMap'));
}

(function ($) {

    // triggered only once (not on barba page change)

    /**
     * Breakpoints
     **/

    var bp = {
        tablet: "768px",
        tabletLandscape: "1024px",
        desktop: "1280px"
    };

    /**
     * Init ScrollMagic Controller
     **/

    var controller = new ScrollMagic.Controller();

    /**
     * Primary Nav
     **/

    var $primaryNav = $('#navPrimary');
    var $primaryNavInner = $('#navPrimaryInner');

    $primaryNavInner.find('a').on('click', function (e) {
        $primaryNavInner.find('.current-menu-item').removeClass('current-menu-item');
        $(this).parent().addClass('current-menu-item');
    });

    //Mobile navigation

    var $primaryNavOverlay = $('#navPrimaryOverlay');

    function closeMenu() {
        TweenMax.set($primaryNavOverlay, {visibility: "hidden"});
        TweenMax.to($primaryNav, 0.5, {x: "100%", force3D: true});
        $('body').removeClass('menuOpen');
    }

    function toggleMenu() {
        if ($('body').hasClass('menuOpen')) {
            closeMenu();
        } else {
            TweenMax.set($primaryNavOverlay, {visibility: "visible"});
            TweenMax.to($primaryNav, 0.5, {x: "0%", force3D: true});
            $('body').addClass('menuOpen');
        }
    }

    $('#navPrimaryToggle').on('click', toggleMenu);
    $('#navPrimaryOverlay').on('touchstart', closeMenu);

    // Footer

    var footerTween = TweenMax.from($('#footerInner'), 10, {opacity: 0, y: 50, force3D: true});

    new ScrollMagic.Scene({
        triggerElement: "#footer",
        duration: $('#footer').innerHeight(),
        triggerHook: 1
    })
        .setTween(footerTween)
        .addTo(controller);

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        //TODO: Undelegate events on barba.js route change
        // All pages
        'common': {
            init: function () {

                /**
                 * Sidebar
                 */

                var scene = new ScrollMagic.Scene({triggerElement: "#sidebarInner", triggerHook: 0})
                    .setPin("#sidebarInner")
                    .addTo(controller);

                /**
                 * Header background image
                 **/

                var $bannerBackground = $('#bannerBackground');
                var $bannerBackgroundColumn;

                function setColumnWidth() {
                    $bannerBackgroundColumn.each(function (i, el) {
                        TweenMax.set($(el).find('.banner__background__column__image'), {backgroundPositionX: -$bannerBackground.width() / 5 * $(el).index() + "px"});
                    });
                }

                var bannerBackgroundScene = {};

                function setBackgroundTween() {
                    TweenMax.killTweensOf($bannerBackgroundColumn);
                    var columnTween = TweenMax.staggerTo($bannerBackgroundColumn, 1, {
                        y: 200,
                        force3D: true,
                        ease: Sine.easeInOut
                    }, -0.2);

                    if (bannerBackgroundScene.remove) {
                        bannerBackgroundScene.remove();
                    }

                    bannerBackgroundScene = new ScrollMagic.Scene({
                        triggerElement: "#bannerBackground",
                        duration: $bannerBackground.height(),
                        triggerHook: 0
                    })
                        .setTween(columnTween)
                        .addTo(controller);
                }

                enquire.register("screen and (min-width:" + bp.tabletLandscape + ")", {
                    match: function () {
                        for (var i = 0; i < 5; i++) {
                            $bannerBackground.append('<div class="js-background-column banner__background__column"><div class="banner__background__column__inner js-background-column-inner"></div></div>');

                            var $tmpColumn = $bannerBackground.find('.js-background-column:last-child .js-background-column-inner');

                            bannerBackgroundUri.forEach(function (uri, i) {
                                $tmpColumn.append('<div class="banner__background__column__image" style="background-image: url(' + uri + '); top: ' + (i * 100) + '%"></div>');
                            });
                        }

                        $bannerBackgroundColumn = $bannerBackground.find('.js-background-column');

                        setColumnWidth();
                        setBackgroundTween();

                        $(window).resize(setColumnWidth);

                        // Main content height

                        if ($('#main').height() < $('#mainContent').height()) {
                            $('#main').css('min-height', $('#mainContent').height() + 280);
                        }
                    },

                    unmatch: function () {
                        $bannerBackgroundColumn.remove();
                        $(window).unbind('resize', setColumnWidth);
                        $('#main').css('min-height', '');
                    },

                    deferSetup: true,

                });

                /*************
                 * Design utility for content
                 */

                $('#main').find('h1, h2, h3, h4, h5, h6').not('.simple').not('.js-no-splittext').each(function (i, el) {
                    $(el).wrapInner('<span></span>').append('<div class="h1__bubble"></div>');
                    new SplitText($(el).find('span, a').last()[0]);
                });

                $('#main').find('p a:not(.btn):not(.js-no-splittext)').each(function (i, el) {
                    new SplitText(el);
                });

                $('.btn:not(.js-no-splittext)').each(function (i, el) {
                    var st = new SplitText(el);

                    var color = "#37D78B";

                    if ($(el).hasClass('btn--outline')) {
                        color = "#000";
                    }

                    $(el).on('mouseenter', function () {
                        TweenMax.killTweensOf(st.chars);
                        TweenMax.staggerFromTo(st.chars, 0.12, {color: "#fff"}, {
                            color: color,
                            ease: Quad.easeInOut
                        }, 0.015);
                    });

                    $(el).on('mouseleave', function () {
                        TweenMax.killTweensOf(st.chars);
                        TweenMax.staggerTo(st.chars, 0.15, {color: "#fff", ease: Quad.easeInOut}, -0.01);
                    });
                });

                $('#main').find('.wpcf7 select').each(function (i, el) {
                    new TleCustomSelect(el);
                });

                // Material inputs

                function checkLabel(e) {
                    $el = $(this);

                    if (($el.val().length >= 0 || e.type === "focusin") && e.type !== "focusout") {
                        $el.closest('.material').addClass('labelOver');
                    } else if ($el.val().length == 0 && e.type === "focusout") {
                        $el.closest('.material').removeClass('labelOver');
                    }
                }

                $('#main').find('.material').find('input, textarea, select').on('input change focusin focusout', checkLabel);

            },
            finalize: function () {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function () {
                // JavaScript to be fired on the home page

                var $bannerSlider = $('#bannerSlider');

                var $bannerBackgroundColumns = $('#bannerBackground').find('.js-background-column-inner');

                $bannerSlider.on('slideChange', function (e, slideIndex, transitionType) {
                    var k = (transitionType === "next") ? 1 : -1;

                    TweenMax.staggerTo($bannerBackgroundColumns, 1.5, {
                        y: -slideIndex * 100 + "%",
                        ease: Quad.easeInOut
                    }, 0.3 * k);
                });

                var bannerSlider = new TleSlider($bannerSlider, {
                    prevArrow: $("#bannerSliderPrev"),
                    nextArrow: $("#bannerSliderNext"),
                    autoplayDelay: 0
                });

                // Content animation

                var scene = new ScrollMagic.Scene({triggerElement: "#main", duration: $(window).height()})
                    .setTween("#introImage", {y: 160, ease: Sine.easeIn})
                    .addTo(controller);
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // Contact page
        'page_template_template_contact': {
            init: function () {
                var s = document.createElement("script");
                s.type = "text/javascript";
                s.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBrbUq7wTo0rYeRyR6Cd-3ZMfCqxP6ps4w&callback=initContactMap";
                $("head").append(s);

                /*******
                 * Form formations
                 */

                var $formationGetValue = $('#getFormation').val();

                if($formationGetValue.length > 3) {
                    $('#your-message').val('Bonjour,\n\nJe suis intéressé par la formation "' + $formationGetValue + '" et souhaiterais en savoir plus...');

                    $('#your-message').focus();
                }

                /*******
                 * Wish cards
                 */

                var $wishCardsWrapper = $('#wishCardsWrapper');

                var tweenStart = Math.max($wishCardsWrapper.offset().top - $(window).height(), 0);
                var tweenLength = tweenStart + $wishCardsWrapper.offset().top + $wishCardsWrapper.height();

                $wishCardsWrapper.height($wishCardsWrapper.height());

                TweenMax.set($wishCardsWrapper.find('.js-wish-card'), {
                    rotationZ: function () {
                        return 90 * (Math.random() - 0.5);
                    }, rotationY: function () {
                        return 30 * (Math.random() - 0.5);
                    }, y: function () {
                        return Math.max(300 * (Math.random()), 100);
                    }
                });

                var scene = new ScrollMagic.Scene({
                    triggerElement: document.body,
                    triggerHook: 0,
                    offset: tweenStart,
                    duration: tweenLength
                })
                    .setTween(TweenMax.to($wishCardsWrapper.find('.js-wish-card'), 1, {
                        rotationZ: function () {
                            return 90 * (Math.random() - 0.5);
                        }, rotationY: function () {
                            return 30 * (Math.random() - 0.5);
                        }, y: function () {
                            return 300 * (Math.random() - 0.7);
                        }, force3D: true
                    }))
                    .addTo(controller);
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        // Plugged with Barba.js newPageReady event
        loadEvents: function (currentStatus, prevStatus, HTMLElementContainer) {
            // Fire common init JS
            UTIL.fire('common');

            HTMLElementContainer = HTMLElementContainer || document.querySelector('.barba-container');
            // Fire page-specific init JS, and then finalize JS
            $.each(HTMLElementContainer.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);


    // Barba.js initialisation

    var $tElWrapper = $('#pageTransitionWrapper');

    TweenMax.set($tElWrapper, {visibility: "hidden"});

    var $tEl = $('#transitionEl');
    var $tElHole = $('#transitionElHole');

    var $tElColumn = $('#pageTransitionWrapper').find('.page-transition-column');

    var tTlOut = new TimelineMax({paused: true})
        .set($tElColumn, {transformOrigin: "50% 100%"})
        .staggerFromTo($tElColumn, 0.4, {scaleY: 0}, {
            scaleY: 1, ease: Quad.easeInOut
        }, 0.15);

    var tTlIn = new TimelineMax({paused: true})
        .set($tElColumn, {transformOrigin: "50% 0"})
        .staggerTo($tElColumn, 0.4, {
            scaleY: 0, ease: Quad.easeInOut
        }, 0.15);

    var FadeTransition = Barba.BaseTransition.extend({
        start: function () {
            /**
             * This function is automatically called as soon the Transition starts
             * this.newContainerLoading is a Promise for the loading of the new container
             * (Barba.js also comes with an handy Promise polyfill!)
             */

            // As soon the loading is finished and the old page is faded out, let's fade the new page
            Promise
                .all([this.newContainerLoading, this.fadeOut()])
                .then(this.done.bind(this))
                .then(this.fadeIn.bind(this));
        },

        fadeOut: function () {
            /**
             * this.oldContainer is the HTMLElement of the old Container
             */
            var _this = this;

            TweenMax.set($tElWrapper, {visibility: "visible"});
            TweenMax.set(document.body, {overflow: "hidden"});

            return new Promise(function (resolve, reject) {
                window.setTimeout(function () {
                    tTlOut.vars.onComplete = function () {
                        //_this.done();
                        resolve("Tween ended");
                    };
                    tTlOut.restart();
                }, 10);
            });
        },

        fadeIn: function () {
            /**
             * this.newContainer is the HTMLElement of the new Container
             * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
             * Please note, newContainer is available just after newContainerLoading is resolved!
             */

            var _this = this;
            var $el = $(this.newContainer);

            $el.css('visibility', 'visible');

            $(this.oldContainer).hide();

            $(window).scrollTop(0);

            tTlIn.vars.onComplete = function () {
                TweenMax.set($tElWrapper, {visibility: "hidden"});
                TweenMax.set(document.body, {overflow: "visible"});
            };

            tTlIn.restart();
        }
    });

    /**
     * Next step, you have to tell Barba to use the new Transition
     */

    Barba.Pjax.getTransition = function () {
        /**
         * Here you can use your own logic!
         * For example you can use different Transition based on the current page or link...
         */

        return FadeTransition;
    };

    Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

    Barba.Pjax.preventCheck = function(evt, element) {
        if (!Barba.Pjax.originalPreventCheck(evt, element)) {
            return false;
        }

        // No need to check for element.href -
        // originalPreventCheck does this for us! (and more!)
        if (/wp-admin/.test(element.href.toLowerCase())) {
            return false;
        }

        return true;
    };

    Barba.Pjax.start();
    Barba.Dispatcher.on('transitionCompleted', UTIL.loadEvents);

    enquire.register("screen and (max-width:" + bp.tabletLandscape + ")", {
        match: function () {
            Barba.Dispatcher.on('linkClicked', closeMenu);
        },

        unmatch: function () {
            Barba.Dispatcher.off('linkClicked', closeMenu);
        },

        deferSetup: true,

    });

})(jQuery); // Fully reference jQuery after this point.
