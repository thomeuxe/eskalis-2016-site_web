var TleCustomSelect = function(el, params) {
    this.el = el;
    this.customEl;
    this.customElList;
    this.listItems;
    this.selectedEl;

    this.isOpen = false;
    this.openClass = "open";
    this.selectedClass = "selected";

    this.init = function() {
        this.hideSelect();
        this.createCustomEl();
        this.el.parentNode.insertBefore(this.customEl, this.el);
    };

    this.hideSelect = function() {
        this.el.style.display = "none";
    };

    this.createCustomEl = function() {
        this.customEl = document.createElement('div');
        this.customEl.className = "custom-select";

        this.selectedEl = document.createElement('div');
        this.selectedEl.className = "custom-select__selected";
        this.customEl.appendChild(this.selectedEl);

        this.customElList = document.createElement('ul');
        this.customElList.className = "custom-select__list";
        this.customEl.appendChild(this.customElList);

        this.updateList();

        this.initListeners();
    };

    this.updateList = function() {
        this.listItems = [];
        this.customElList.innerHTML = "";

        for (var i = 0; i < this.el.children.length; i++) {
            this.listItems.push(document.createElement('div'));
            this.listItems[this.listItems.length - 1].className = "custom-select__item";
            this.listItems[this.listItems.length - 1].innerHTML = this.el.children[i].textContent;

            this.initItemListeners(this.listItems[this.listItems.length - 1], this.el.children[i]);

            this.customElList.appendChild(this.listItems[this.listItems.length - 1]);
        }

    };

    this.initListeners = function() {
        this.customEl.addEventListener('click', this.toggleOptions.bind(this));
        document.body.addEventListener('click', this.closeOptions.bind(this));
    };

    this.initItemListeners = function(customItem, origItem) {
        customItem.addEventListener('click', function(e) {
            this.change(e, origItem);
        }.bind(this), false);
    };

    this.toggleOptions = function(e) {
        e.stopPropagation();
        
        if(this.isOpen) {
            this.closeOptions();
        } else {
            this.openOptions();
        }
    };

    this.openOptions = function() {
        this.isOpen = true;

        if (this.customEl.classList) {
            this.customEl.classList.add(this.openClass);
        } else {
            this.customEl.className += ' ' + this.openClass;
        }
    };

    this.closeOptions = function() {
        this.isOpen = false;

        if (this.customEl.classList) {
            this.customEl.classList.remove(this.openClass);
        } else {
            this.customEl.className = this.customEl.className.replace(new RegExp('(^|\\b)' + this.openClass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    };

    this.change = function(e, origItem) {
        e.stopPropagation();

        for (var i = 0; i < this.customElList.children.length; i++) {
            if (this.customElList.children[i].classList) {
                this.customElList.children[i].classList.remove(this.selectedClass);
            } else {
                this.customElList.children[i].className = this.customElList.children[i].className.replace(new RegExp('(^|\\b)' + this.selectedClass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
        }

        this.el.selectedIndex = Array.prototype.slice.call( this.el.children ).indexOf(origItem);

        if (this.customElList.children[this.el.selectedIndex].classList) {
            this.customElList.children[this.el.selectedIndex].classList.add(this.selectedClass);
        } else {
            this.customElList.children[this.el.selectedIndex].className += ' ' + this.selectedClass;
        }

        this.selectedEl.innerHTML = origItem.innerHTML;

        var event = document.createEvent('HTMLEvents');
        event.initEvent('change', true, false);
        this.el.dispatchEvent(event);

        this.closeOptions();
    };

    this.init();
};