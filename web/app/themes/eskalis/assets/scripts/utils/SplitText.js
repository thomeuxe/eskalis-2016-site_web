/**************************
 * SplitText
 *
 * Split inline text into div for words, chars and spaces (usufull for tweening)
 * Inspired by GSAP SplitText
 * By Thomas LECOEUR
 *
 * COMPATIBILITY : IE9+
 * DEPENDENCY : NONE
 *
 * @param el
 * @constructor
 */

var SplitText = function(el) {

    this.chars = [];
    this.words = [];
    this.space = [];

    this.setChildren = function() {
        this.chars = el.querySelectorAll('.char');
        this.words = el.querySelectorAll('.word');
        this.space = el.querySelectorAll('.space');
    };

    this.update = function() {
        var html = "";

        var str = el.textContent;
        str = str.replace(/(?:\r\n|\r|\n)/g, 'þ');

        html += "<div class='word'>";

        var isHtmlTag = false;


        for (var i = 0, len = str.length; i < len; i++) {
            if(str[i] == 'þ') {
                html += "</div><br/>";
                html += "<div class='word'>";
            } else if (str[i] === ' ') {
                html += "<div class='space'>&nbsp;</div>";
                html += "</div>";
                html += "<div class='word'>";
            } else if(str[i] === '>') {
                isHtmlTag = false;
            } else if(str[i] === '<' || isHtmlTag == true) {
                isHtmlTag = true;
            } else {
                html += "<div class='char'>" + str[i] + "</div>";
            }
        }

        html += "</div>";

        el.innerHTML = html;

        var childs = el.querySelectorAll('*');
        Array.prototype.forEach.call(childs, function(el, i){
            el.style.display = 'inline-block';
        });

        this.setChildren();
    };

    this.update();
};