/***********************
 *
 * Custom slider for Eskalis
 * made by Thomas Lecoeur
 *
 * @param el
 * @param params
 * @constructor
 */

var TleSlider;


jQuery(function($) {
    TleSlider = function(el, params) {
        params = (typeof params !== "object") ? {} : params;

        this.$prevArrow = params.prevArrow;
        this.$nextArrow = params.nextArrow;
        this.autoplayDelay = params.autoplayDelay || 0;

        if(el instanceof jQuery) {
            this.$el = el;
        } else {
            this.$el = $(el);
        }

        this.$children = this.$el.children();
        this.isTweening = false;

        this.init = function() {
            TweenMax.set(this.$children, {display: "none"});

            this.$active = this.$children.first();

            this.$children.each(function(i, el) {
                $el = $(el);
                el.splitTitle = new SplitText($el.find('.js-slide-title')[0]);
                el.splitSubtitle = new SplitText($el.find('.js-slide-subtitle')[0]);
            });

            this.initListeners();


            TweenMax.delayedCall(1, function() {
                if(this.autoplayDelay != 0) {
                    this.autoplay(true);
                }

                this.showSlide(this.$active);
            }.bind(this));
        };

        this.initListeners = function() {
            if(this.$prevArrow && this.$prevArrow.length) {
                this.$prevArrow.on('click', this.goPrev.bind(this));
            }

            if(this.$nextArrow && this.$nextArrow.length) {
                this.$nextArrow.on('click', this.goNext.bind(this));
            }

            $(document).on('keydown', function(e) {
                if(e.which == 37 || e.which == 39) {
                    e.preventDefault();

                    if(e.which == 37) {
                        this.goPrev();
                    }

                    if(e.which == 39) {
                        this.goNext();
                    }
                }
            }.bind(this));
        };

        this.goNext = function() {
            if(!this.isTweening) {
                this.$prevActive = this.$active;

                if (this.$active.next().length) {
                    this.$active = this.$active.next();
                } else {
                    this.$active = this.$children.first();
                }

                this.transitSlide("next");
            }
        };

        this.goPrev = function() {
            if(!this.isTweening) {
                this.$prevActive = this.$active;

                if (this.$active.prev().length) {
                    this.$active = this.$active.prev();
                } else {
                    this.$active = this.$children.last();
                }

                this.transitSlide("prev");
            }
        };

        this.transitSlide = function(type) {
            TweenMax.killChildTweensOf([this.$prevActive, this.$active]);

            this.$el.trigger('slideChange', [this.$active.index(), type]);

            this.hideSlide(this.$prevActive, function() {
                this.showSlide(this.$active);
            }.bind(this));
        };

        this.showSlide = function($slideEl, callback) {
            this.isTweening = true;
            TweenMax.set($slideEl, {display: "block", className: "+=active"});

            this.$el.trigger('showSlide', [$slideEl.index()]);

            var tl = new TimelineMax({
                onComplete: function() {
                    this.isTweening = false
                }.bind(this)
            });

            tl.staggerFromTo($slideEl[0].splitTitle.chars, 0.6, {y: "100%"}, {y: "0%", ease: Quad.easeOut}, 0.022);
            tl.fromTo($slideEl.find('.js-slide-subtitle'), 0.5, {y: 20, opacity: 0.01, ease: Quad.easeOut}, {y: 0, opacity: 1}, "-=0.4");
            tl.fromTo($slideEl.find('.js-slide-btn'), 0.5, {y: 30, opacity: 0.01}, {y: 0, opacity: 1, ease: Quad.easeOut}, "-=0.4");
        };

        this.hideSlide = function($slideEl, callback) {
            this.isTweening = true;

            var tl = new TimelineMax({onComplete: function() {
                TweenMax.set($slideEl, {display: "none", className: "-=active"});
                this.isTweening = false;
                callback();
            }.bind(this)});

            tl.staggerTo($slideEl[0].splitTitle.chars, 0.4, {y: "100%", ease: Quad.easeIn}, 0.016);
            tl.to($slideEl.find('.js-slide-subtitle'), 0.3, {y: 30, opacity: 0.01, ease: Quad.easeIn}, "-=0.3");
            tl.to($slideEl.find('.js-slide-btn'), 0.3, {y: 30, opacity: 0.01, ease: Quad.easeIn}, "-=0.25");

        };

        this.autoplay = function(isInit) {
            if(!isInit) {
                this.goNext();
            }

            TweenMax.delayedCall(this.autoplayDelay, this.autoplay.bind(this));
        };

        this.init();
    };
});
