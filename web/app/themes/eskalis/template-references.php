<?php
/**
 * Template Name: References Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/cpt-reference-list'); ?>
<?php endwhile; ?>
