<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <div id="pageTransitionWrapper" class="page-transition-wrapper">
      <div class="page-transition-column"></div>
      <div class="page-transition-column"></div>
      <div class="page-transition-column"></div>
      <div class="page-transition-column"></div>
      <div class="page-transition-column"></div>
    </div>
    <div class="tle-layout">
      <div id="barba-wrapper" class="wrap">
        <div <?php body_class("barba-container"); ?>>
          <?php
            do_action('get_header');
          ?>
          <div role="document">
            <?php
            get_template_part('templates/header');
            ?>
            <div id="mainContent" class="content tle-content row">
              <?php if (Setup\display_sidebar()) : ?>
                <aside class="sidebar">
                  <div id="sidebarInner" class="sidebar__inner">
                    <?php include Wrapper\sidebar_path(); ?>
                  </div>
                </aside><!-- /.sidebar -->
              <?php endif; ?>
              <main id="main" class="main">
                <?php include Wrapper\template_path(); ?>
              </main><!-- /.main -->
            </div><!-- /.content -->
          </div><!-- /.wrap -->
        </div>
      </div>

      <?php
      
      get_template_part('templates/nav-primary');

      ?>

      <?php
        do_action('get_footer');
        get_template_part('templates/footer');
        wp_footer();
      ?>
    </div>
  </body>
</html>
