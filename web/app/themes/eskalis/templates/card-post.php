<article <?php post_class('post-card'); ?>>
    <a class="post-card__link" href="<?php the_permalink(); ?>">
        <div class="post-card__thumbnail-wrapper">
            <?php
            the_post_thumbnail('post-card');
            ?>
        </div>

        <header class="post-card__header">
            <div class="post-card__date">
                Publié le <?php echo get_the_date(); ?>
            </div>
            <div class="post-card__title">
                <?php the_title(); ?>
            </div>
        </header>
    </a>
</article>