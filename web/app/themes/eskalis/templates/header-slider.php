<div class="banner__slider__wrapper">
    <ul id="bannerSlider" class="banner__slider">
        <?php
        foreach (CFS()->get('header_slides') as $slide) {
            ?>
                <li class="banner__slide">
                    <h2 class="banner__slider__title js-slide-title"><?php echo $slide['title']; ?></h2>
                    <p class="banner__slider__subtitle js-slide-subtitle"><?php echo $slide['excerpt']; ?></p>
                    <a href="<?php echo $slide['link']['url']; ?>"<?php if($slide['link']['target'] != "none"): ?> target="<?php echo $slide['link']['target']; ?>"<?php endif; ?> class="btn btn--outline js-slide-btn"><?php echo $slide['link']['text']; ?></a>
                </li>
            <?php
        }
        ?>
    </ul>
    <div class="banner__slider__navigation">
        <button id="bannerSliderPrev" class="banner__slider__nav banner__slider__nav--prev"></button>
        <button id="bannerSliderNext" class="banner__slider__nav banner__slider__nav--next"></button>
    </div>
</div>