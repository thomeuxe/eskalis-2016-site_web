<header class="page-header">
    <h1>Nos références</h1>
</header>

<?php

$reference_sectors = get_terms(array(
    'taxonomy' => 'esk_reference_sector',
    'hide_empty' => true,
));

the_content();

foreach ($reference_sectors as $sector) {
    $postQuery = new WP_Query(array(
        'post_type' => 'esk_reference',
        'tax_query' => array(
            array(
                'taxonomy' => 'esk_reference_sector',
                'field' => 'slug',
                'terms' => array($sector->slug),
                'operator' => 'IN'
            )
        ),
        'posts_per_page' => -1
    ));
    ?>
    <h2><?php echo $sector->name; ?></h2>
    <?php

    if ($postQuery->have_posts()) :

        ?>
            <ul class="references__wrapper">
                <?php

                $i = 0;

                while ($postQuery->have_posts()) :
                    $postQuery->the_post();

                    if ($i % 4 == 0) {
                        echo '<div class="references__row">';
                    }

                    $cat = wp_get_object_terms(get_the_ID(), 'esk_reference_sector');

                    ?>

                    <li class="references__item" data-cat="<?php
                    $j = 0;
                    foreach ($cat as $catItem) {
                        if ($j != 0) {
                            echo ", ";
                        }
                        echo $catItem->name;
                        $j++;
                    }
                    ?>">
                        <div class="img-wrapper">
                            <?php the_post_thumbnail('reference'); ?>
                        </div>
                    </li>

                    <?php

                    if (($i + 1) % 4 == 0) {
                        echo '</div>';
                    }

                    $i++;
                endwhile;

                /* Restore original Post Data */
                wp_reset_postdata();

                ?>
            </ul>
        <?php
    endif; ?>
    <?php
    // Reset things, for good measure
    $member_group_query = null;
    wp_reset_postdata();
}