<header class="banner">
  <div class="banner__brand">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/header/eskalis-logo.svg" width="165" height="85" alt="<?php bloginfo('name'); ?>"></a>
  </div>
  <?php

  if(CFS()->get('header_slides') != "") {
    get_template_part('templates/header', 'slider');
  }

  ?>

  <script>
    var bannerBackgroundUri = [<?php
        $frontpageID = get_option( 'page_on_front' );

      $singleHeaderBackground = CFS()->get('single_header_background');

      if($singleHeaderBackground != "") {
        $imgData = wp_get_attachment_image_src($singleHeaderBackground, 'header');
        echo '"' . $imgData[0] . '",';
      } else if(CFS()->get('header_slides', $frontpageID) != "") {
        foreach (CFS()->get('header_slides', $frontpageID) as $slide) {
          if($slide['picture'] != "") {
            $imgData = wp_get_attachment_image_src($slide['picture'], 'header');
            echo '"' . $imgData[0] . '",';
          } else {
            echo '"' . get_stylesheet_directory_uri() . '/assets/images/header/background.jpg",';
          }
        }
      } else {
        echo '"' . get_stylesheet_directory_uri() . '/assets/images/header/background.jpg",';
      }
    ?>];
  </script>
  
  <div class="v-line__wrapper">
    <div class="v-line"></div>
    <div class="v-line"></div>
    <div class="v-line"></div>
    <div class="v-line"></div>
  </div>
  <div id="bannerBackground" class="banner__background"></div>
</header>