<?php use Roots\Sage\Titles; ?>

<header class="page-header">
  <h1><?= post_type_archive_title(); ?></h1>
</header>
