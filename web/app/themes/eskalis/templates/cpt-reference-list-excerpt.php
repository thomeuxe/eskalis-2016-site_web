<?php

$postQuery = new WP_Query([
    'post_type' => 'esk_reference',
    'posts_per_page' => 8,
    'meta_query' => [
        [
            'key' => 'featured',
            'value' => true
        ]
    ]
]);

if ($postQuery->have_posts()) :

?>
<section class="section section--reference">
    <h2 class="h1">Nos références</h2>

    <ul class="references__wrapper">
        <?php

        $i = 0;

        while ($postQuery->have_posts()) :
            $postQuery->the_post();

            if ($i % 4 == 0) {
                echo '<div class="references__row">';
            }

            $cat = wp_get_object_terms(get_the_ID(), 'esk_reference_sector');

            ?>

            <li class="references__item" data-cat="<?php
            $j = 0;
            foreach ($cat as $catItem) {
                if($j != 0) {
                    echo ", ";
                }
                echo $catItem->name;
                $j++;
            }
            ?>">
                <div class="img-wrapper">
                    <?php the_post_thumbnail('reference'); ?>
                </div>
            </li>

            <?php

            if (($i + 1) % 4 == 0) {
                echo '</div>';
            }

            $i++;
        endwhile;

        /* Restore original Post Data */
        wp_reset_postdata();

        ?>
    </ul>

    <div class="references__btn-wrapper">
        <a class="btn" href="<?php echo get_permalink(cfs_get_option( 'global_options', 'references_page' )[0]); ?>">Voir toutes nos références</a>
    </div>

</section>
<?php
endif;