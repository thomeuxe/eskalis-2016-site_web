<header class="page-header">
  <h1>Nos formations</h1>
</header>

<?php

$reference_sectors = get_terms(array(
  'taxonomy'   => 'esk_formation_category',
  'hide_empty' => TRUE,
));

?>

<div class="formations__wrapper">

  <div class="formations__row">
    <div class="formations__title"></div>
    <div class="formations__target formations__header">
      <p class="formations__center">
        Pour qui ?
      </p>
    </div>
    <div class="formations__times formations__header">
      <p class="formations__center">
        Quand ?
      </p>
    </div>
    <div class="formations__price formations__header">
      <p class="formations__center">
        Combien ?
      </p>
    </div>
  </div>

  <?php

  foreach ($reference_sectors as $sector) {
    $postQuery = new WP_Query(array(
      'post_type'      => 'esk_formation',
      'tax_query'      => array(
        array(
          'taxonomy' => 'esk_formation_category',
          'field'    => 'slug',
          'terms'    => array($sector->slug),
          'operator' => 'IN'
        )
      ),
      'posts_per_page' => -1
    ));
    ?>
    <h2 class="formations__category simple">
      <?php echo $sector->name; ?>
    </h2>
    <?php

    if ($postQuery->have_posts()) :


      while ($postQuery->have_posts()) :
        $postQuery->the_post();
        ?>
        <article id="<?php echo $post->post_name; ?>" class="formations__row">
          <h3 class="formations__title simple">
            <div class="formations__center">
              <?php the_title(); ?>
            </div>
          </h3>
          <div class="formations__target">
            <p class="formations__center">
              <span class="screen-reader-text">Cible :</span>
              <?php echo CFS()->get('target'); ?>
            </p>
          </div>
          <div class="formations__times">
            <div class="formations__center">
              <p class="formations__duration">
                <?php echo CFS()->get('duration'); ?>
              </p>
              <p class="formations__dates"><?php

                $dates = CFS()->get('dates');
                $now   = new DateTime('now');

                if ($dates != "") {
                  foreach ($dates as $date) {
                    $d = new DateTime($date['date']);

                    if ($d->getTimestamp() > $now->getTimestamp()):
                      ?>
                      <span class="formations__date">
                                            <?php
                                            echo $d->format('d/m/Y');
                                            ?>
                                        </span>
                      <?php
                    endif;
                  }
                }
                else {
                  ?>
                  <span
                    class="formations__date">Nous consulter pour la date</span>
                  <?php
                }
                ?></p>
            </div>
          </div>
          <div class="formations__price">
            <p class="formations__center">
              <?php echo CFS()->get('price'); ?>
            </p>
          </div>
          <div class="formations__download">
            <div class="formations__center">
              <?php if (CFS()->get('pdf') !== ''): ?>
                <a href="<?php echo CFS()->get('pdf'); ?>" download
                   class="btn btn--circle btn--pdf">PDF</a>
                <?php
              else:
                ?>
                <a disabled="disabled" aria-disabled="true" download
                   class="btn btn--circle btn--pdf btn--disabled js-no-splittext">PDF</a>
                <?php
              endif;
              ?>
            </div>
            <div class="formations__buttons">
              <a
                href="<?php echo get_permalink(cfs_get_option('global_options', 'contact_page')[0]); ?>?formation=<?php echo the_title(); ?>"
                class="btn btn--circle btn--book">quesions</a>
              <?php
              if (CFS()->get('pdf_application')): ?>
                <a target="_blank"
                   href="<?php echo CFS()->get('pdf_application'); ?>" download
                   class="btn btn--circle btn--share">participer</a>
                <?php
              else:
                ?>
                <a disabled="disabled" aria-disabled="true" download
                   class="btn btn--circle btn--apply btn--disabled">participer</a>
                <?php
              endif;
              ?>
            </div>
          </div>
        </article>
        <?php
      endwhile;

      /* Restore original Post Data */
      wp_reset_postdata();

      ?>
      </tr>
      <?php

    endif;

    // Reset things, for good measure
    $member_group_query = NULL;
    wp_reset_postdata();
  }

  ?>
</div>

<?php

the_content();

?>