<!-- Introduction -->

<section class="section section--intro">
    <h1 class="intro__title"><?php echo CFS()->get('introduction_title'); ?></h1>

    <div class="intro__block">
        <div class="intro__block__content">
            <?php echo CFS()->get('introduction'); ?>
            <div class="intro__block__points"></div>
        </div>
        <div id="introImage" class="intro__block__image">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/homepage/nous-sommes.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/homepage/nous-sommes@2x.jpg 2x" alt="">
        </div>
    </div>
</section>

<!-- END - Introduction -->

<!-- CTA Rappel -->

<section class="section section--full section--padding section--no-before section--rappel">
    <div class="rappel__wrapper">
        <p class="rappel__title">
            <?php echo CFS()->get('cta'); ?>
        </p>
        <p class="rappel__btn-wrapper">
            <a href="<?php echo get_permalink(cfs_get_option( 'global_options', 'contact_page' )[0]); ?>" class="btn btn--outline">Contactez Eskalis</a>
        </p>
    </div>
</section>

<!-- END - CTA Rappel -->

<!-- Reference -->

<?php

get_template_part('templates/cpt-reference-list', 'excerpt');

?>

<!-- END - Reference -->

<!-- News -->

<?php

$postQuery = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 3
]);

if ($postQuery->have_posts()) :

    ?>
    <section class="section section--news">
        <h2 class="h1">Les Actualités</h2>

        <div class="news-wrapper">
            <?php

            while ($postQuery->have_posts()) :
                $postQuery->the_post();

                get_template_part('templates/card', 'post');
            endwhile;

            /* Restore original Post Data */
            wp_reset_postdata();

            ?>
        </div>

        <div class="news__btn-wrapper">
            <a class="btn" href="<?php echo get_post_type_archive_link('post'); ?>">Voir toutes les actualités RH</a>
        </div>

    </section>
    <?php
endif;

?>

<!-- END - News -->