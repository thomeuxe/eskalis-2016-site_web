<footer id="footer" class="content-info footer">
  <div id="footerInner" class="footer__inner">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
