<header class="page-header">
    <h1><?= the_title(); ?></h1>
</header>

<!-- Introduction -->
<?php
if (CFS()->get('intro') != ""):
    ?>
    <div class="contact__intro">
        <?php
        echo CFS()->get('intro');
        ?>
    </div>
<?php endif; ?>

<!-- END - Introduction -->

<!-- Contact form -->

<?php
if (CFS()->get('wpcf7') != ""):
    ?>
    <div class="contact__form">
        <?php
        echo do_shortcode(CFS()->get('wpcf7'));
        ?>
    </div>
<?php endif; ?>

<div class="contact__lateral">
    <?php
    //echo CFS()->get('form_text');
    ?>

    <div id="wishCardsWrapper" class="contact__wish-cards">
        <?php
        $cards = cfs_get_option('global_options', 'wish_cards');

        foreach ($cards as $card) {
            if ($card['display_contact']):
                ?>
                <div class="wish-card js-wish-card">
                    <div class="wish-card__inner">
                        <?php echo wp_get_attachment_image($card['recto'], 'wish-card-thumbnail', false, ["class" => "wish-card__verso"]); ?>
                        <?php echo wp_get_attachment_image($card['verso'], 'wish-card-thumbnail', false, ["class" => "wish-card__recto"]); ?>
                    </div>
                </div>
                <?php
            endif;
        }
        ?>
    </div>
</div>

<div class="contact__map">
    <div id="contactMap" class="contact__map__embed"></div>
    <a href="https://www.google.com/maps/place/3+Impasse+des+Prairies,+74940+Annecy-le-Vieux,+France/@45.9324606,6.1668169,17z/data=!3m1!4b1!4m5!3m4!1s0x478b8f0178af3bb7:0x31ab38e2bd3d513a!8m2!3d45.9324606!4d6.1690056?hl=fr-FR"
       target="_blank" class="btn contact__map__btn">Ouvrir dans Google Maps</a>
</div>

<!-- END - Contact form -->