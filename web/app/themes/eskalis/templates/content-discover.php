<header class="page-header">
    <h1><?= the_title(); ?></h1>
</header>

<!-- Introduction Eskalis -->

<?php

echo CFS()->get('intro');

?>

<!-- END - Introduction Eskalis -->

<!-- Introduction Consultants -->

<section>
    <h2>Le réseau Eskalis</h2>
    <div class="consultants-wrapper">

        <?php

        $consultants = CFS()->get('consultants');

        foreach ($consultants as $consultant) {
            ?>
            <div class="consultant">
                <div class="consultant__pic"><?php echo wp_get_attachment_image($consultant['picture'], 'consultant'); ?></div>
                <div class="consultant__content">
                    <div class="consultant__company"><?php echo $consultant['function']; ?>, <?php echo $consultant['company']; ?></div>
                    <div class="consultant__name"><?php echo $consultant['name']; ?></div>
                    <div class="consultant__mail"><?php echo $consultant['mail']; ?></div>
                    <div class="consultant__description"><?php echo $consultant['description']; ?></div>
                    <div class="consultant__btn">
                        <a href="<?php echo get_permalink(cfs_get_option( 'global_options', 'contact_page' )[0]); ?>?consultant=<?php echo $consultant['mail']; ?>" class="btn btn--small">Contacter</a>
                    </div>
                </div>
            </div>
            <?php
        }

        ?>
    </div>
</section>

<!-- END - Introduction Consultants -->