<nav class="nav-primary" id="navPrimary">
  <div class="nav-primary__toggle" id="navPrimaryToggle">
    <div class="nav-primary__burger"></div>
  </div>
  <div class="nav-primary__inner nav-primary__inner--focus" id="navPrimaryInner">
    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu([
          'theme_location' => 'primary_navigation',
          'menu_class' => 'nav',
          'walker' => new Roots\Sage\Extras\Esk_Main_Walker_Nav_Menu()
      ]);
    endif;
    ?>
  </div>
  <?php dynamic_sidebar('sidebar-nav'); ?>
  <div class="nav-primary__inner">
    <a class="nav-primary__logo" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/header/eskalis-logo.svg" width="165" height="85" alt="<?php bloginfo('name'); ?>"></a>
  </div>
</nav>
<div class="nav-primary__overlay" id="navPrimaryOverlay"></div>
