<?php get_template_part('templates/page', 'header-archive'); ?>

<div class="news-wrapper">
    <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
    <?php endwhile; ?>
</div>
