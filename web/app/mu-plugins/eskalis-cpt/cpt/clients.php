<?php

/************
 * Client post type
 */
function eskalis_client_cpt() {

    $labels = array(
        'name'                  => _x( 'Clients', 'Post Type General Name' ),
        'singular_name'         => _x( 'Client', 'Post Type Singular Name' ),
        'menu_name'             => __( 'Clients' ),
        'name_admin_bar'        => __( 'Client' ),
        'all_items'             => __( 'Tous les clients' ),
        'new_item'              => __( 'Nouveau client' ),
        'update_item'           => __( 'Mettre à jour le client' ),
        'view_item'             => __( 'Voir le client' ),
    );
    $args = array(
        'label'                 => __( 'Client' ),
        'description'           => __( 'Eskalis clients' ),
        'labels'                => $labels,
        'supports'              => array( 'title', ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-businessman',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'capability_type'       => 'page',
    );
    register_post_type( 'esk_client', $args );

}
add_action( 'init', 'eskalis_client_cpt', 0 );
