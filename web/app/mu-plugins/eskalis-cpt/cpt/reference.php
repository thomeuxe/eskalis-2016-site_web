<?php

/************
 * Client post type
 */
function eskalis_reference_cpt() {

    $labels = array(
        'name'                  => __( 'Nos Références' ),
        'singular_name'         => __( 'Référence' ),
        'menu_name'             => __( 'Références' ),
        'name_admin_bar'        => __( 'Référence' ),
        'all_items'             => __( 'Toutes les références' ),
        'new_item'              => __( 'Nouvelle référence' ),
        'update_item'           => __( 'Mettre à jour la référence' ),
        'view_item'             => __( 'Voir la référence' ),
    );
    $args = array(
        'label'                 => __( 'Référence' ),
        'description'           => __( 'Eskalis référence' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-awards',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'capability_type'       => 'page',
    );
    register_post_type( 'esk_reference', $args );
}
add_action( 'init', 'eskalis_reference_cpt', 0 );

// Register Custom Taxonomy
function eskalis_reference_sector() {

    $labels = array(
        'name'                       => _x( 'Secteur d\'activité', 'Secteur d\'activité' ),
        'singular_name'              => _x( 'Secteur d\'activité', 'Taxonomy Singular Name' ),
        'menu_name'                  => __( 'Secteur d\'activité' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'esk_reference_sector', array( 'esk_reference' ), $args );

}
add_action( 'init', 'eskalis_reference_sector', 0 );