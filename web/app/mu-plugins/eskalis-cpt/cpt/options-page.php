<?php

/**
 * Generate Option page working with CFS Options Screens
 *
 * @param $screens
 * @return array
 */

function esk_options_screens($screens ) {
    $screens[] = array(
        'name'            => 'global_options',
        'menu_title'      => __( 'Données générales' ),
        'page_title'      => __( 'Options globales du site' ),
        'menu_position'   => 4,
        'icon'            => 'dashicons-clipboard', // optional, dashicons-admin-generic is the default
        'field_groups'    => array( 5 ), // post ID(s) of CFS Field Group to use on this page
    );
    return $screens;
}

add_filter( 'cfs_options_screens', 'esk_options_screens' );
