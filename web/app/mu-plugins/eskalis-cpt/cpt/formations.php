<?php

/************
 * Formation post type
 */
function eskalis_formation_cpt() {

    $labels = array(
        'name'                  => _x( 'Formations', 'Post Type General Name' ),
        'singular_name'         => _x( 'Formation', 'Post Type Singular Name' ),
        'menu_name'             => __( 'Formations' ),
        'name_admin_bar'        => __( 'Formation' ),
        'all_items'             => __( 'Toutes les formations' ),
        'new_item'              => __( 'Nouvelle formation' ),
        'update_item'           => __( 'Mettre à jour la formation' ),
        'view_item'             => __( 'Voir la formation' ),
    );
    $rewrite = array(
        'slug'                  => 'formations',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __( 'Formation' ),
        'description'           => __( 'Eskalis formations' ),
        'labels'                => $labels,
        'supports'              => array( 'title', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-welcome-learn-more',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => false,
        //'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'esk_formation', $args );

}
add_action( 'init', 'eskalis_formation_cpt', 0 );

// Register Custom Taxonomy
function eskalis_formation_category() {

    $labels = array(
        'name'                       => _x( 'Catégorie de formation', 'Catégorie de formation' ),
        'singular_name'              => _x( 'Catégorie de formation', 'Taxonomy Singular Name' ),
        'menu_name'                  => __( 'Catégorie de formation' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'esk_formation_category', array( 'esk_formation' ), $args );

}
add_action( 'init', 'eskalis_formation_category', 0 );

// FILTER SEARCH RESULT TO DISPLAY BETTER THE FORMAIONS

function eskalis_filter_formation_search_results( $posts ) {
  if(is_search() && is_main_query() && !is_admin()) {
    foreach ( $posts as $post ) {
      if ( $post->post_type == 'esk_formation') {
        $post->post_title = "Formation : " . $post->post_title;

        if(function_exists('CFS')) {
          $post->post_excerpt = "<strong>Pour qui ?</strong> " . CFS()->get('target', $post->ID);
        }
      }
    }
  }
  return $posts ;
}
add_filter( 'posts_results', 'eskalis_filter_formation_search_results', 10, 2 );

// FILTER THE FORMAION PERMALINK TO GET THE PAGE LINK (AS THERE IS NO SINGLE PAGE)

function eskalis_formation_permalink_archive( $url, $post, $leavename=false ) {
  if ( get_post_type($post) == 'esk_formation' ) {
    $url = get_permalink(cfs_get_option( 'global_options', 'formations_page' )[0]);;
  }

  return $url;
}
add_filter( 'post_type_link', 'eskalis_formation_permalink_archive' );