<?php
/**
 * Plugin Name: Eskalis | Custom Post Type
 * Plugin URI: http://eskalis.com
 * Description: This plugin create Eskalis Custom Post Types
 * Version: 0.0.1
 * Author: Thomas LECOEUR
 * Author URI: http://thomaslecoeur.com
 * License: GPL2
 */

include_once "cpt/formations.php";
//include_once "cpt/clients.php";
include_once "cpt/reference.php";

include_once "cpt/options-page.php";